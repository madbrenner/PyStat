__author__ = 'matt'

import logging
logger = logging.getLogger(__name__)

import threading
import time

import adafruit_mcp9808
import board
import busio

from utils.thermometers.TemperatureReader import TemperatureReader

# A class that provides the current temperature to a requestor.  Updates the
# temperature every 10 seconds
class MCP9808(TemperatureReader):
    def __init__(self, configuration):
        i2c = busio.I2C(board.SCL, board.SDA)
        self.mcp_sensor = adafruit_mcp9808.MCP9808(i2c)

        super().__init__(configuration)

    def get_current_temperature(self):
        # check thermometer
        try:
            temperature = self.mcp_sensor.temperature
            fahrenheit = round((temperature * 1.8) + 32, 1)

            return fahrenheit
        except:
            logger.info("reading thermometer failed, returning None")
            return None
