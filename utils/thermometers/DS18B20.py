__author__ = 'matt'

import logging
logger = logging.getLogger(__name__)

import threading
import time

from utils.thermometers.TemperatureReader import TemperatureReader

# A class that provides the current temperature to a requestor.  Updates the
# temperature every 10 seconds
class DS18B20(TemperatureReader):
    def __init__(self, configuration):
        super().__init__(configuration)

    def get_current_temperature(self):
        # check thermometer
        try:
            f = open("/sys/bus/w1/devices/28-0000041117ab/w1_slave")
            f.readline()
            line = f.readline()
            split = line.split("=")

            temperature = float(split[1]) / 1000
            fahrenheit = round((temperature * 1.8) + 32, 1)

            f.close()
            return fahrenheit
        except:
            logger.info("reading thermometer failed, returning None")
            return None
