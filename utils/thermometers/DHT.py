__author__ = 'matt'

import logging
logger = logging.getLogger(__name__)

import threading
import time

import Adafruit_DHT

from utils.thermometers.TemperatureReader import TemperatureReader

# A class that provides the current temperature to a requestor.  Updates the
# temperature every 10 seconds
class DHT(TemperatureReader):
    def __init__(self, configuration):
        # determine if it is a DHT11 or DHT22
        model = int(configuration.thermometer.strip("DHT"))
        self.model = model
        self.pin = configuration.thermometerPin

        super().__init__(configuration)

    def get_current_temperature(self):
        # check thermometer
        try:
            humidity, temperature = Adafruit_DHT.read_retry(self.model, self.pin)
            fahrenheit = round((temperature * 1.8) + 32, 1)

            return fahrenheit
        except:
            logger.info("reading thermometer failed, returning None")
            return None
