__author__ = 'matt'

import logging
logger = logging.getLogger(__name__)

import threading
import time

class TemperatureReader():
    def __init__(self, configuration):
        self.lastCheck = time.time()
        self.currentLock = threading.Lock()
        self.tempFile = configuration.thermometerTestFile
        self.currentTemperature = self.get_current_temperature()

    def get_current_temperature(self):
        temperature = None

        try:
            testTempFile = open(self.tempFile)
            if testTempFile.readable():
                temperature = round(float(testTempFile.read()), 1)

            testTempFile.close()
        except FileNotFoundError:
            logger.info(f"Could not find {self.tempFile}, returning None")
        except ValueError:
            logger.info(f"Error parsing value in {self.tempFile} to float, returning None")

        return temperature

    def CurrentTemperature(self):
        if self.currentTemperature is None or 10 <= time.time() - self.lastCheck:
            self.currentLock.acquire()
            self.currentTemperature = self.get_current_temperature()
            self.lastCheck = time.time()
            self.currentLock.release()

        return self.currentTemperature
