__author__ = 'matt'

import logging
logger = logging.getLogger(__name__)

import threading
from scheduler.ScheduleHelper import ScheduleEvent
from utils.ThermostatConfiguration import ThermostatConfiguration

class FileManager():
    def __init__(self, configurationFileName, currentFileName, scheduleFileName):
        self.configurationFileName = configurationFileName
        self.currentFileName = currentFileName
        self.scheduleFileName = scheduleFileName
        self.configuration = None
        self.scheduleHelper = None

        self.configurationLock = threading.Lock()
        self.currentLock = threading.Lock()
        self.scheduleLock = threading.Lock()

    def read_configuration(self):
        self.configurationLock.acquire()
        self.configuration = ThermostatConfiguration()

        try:
            configurationFile = open(self.configurationFileName)

            if configurationFile.readable():
                for line in configurationFile:
                    line = line.strip()
                    if "" != line and "#" != line[0]:
                        pair = line.split()
                        if 1 < len(pair):
                            self.set_configuration_option(pair[0], pair[1], self.configuration)

            configurationFile.close()
        except FileNotFoundError as e:
            logger.info(f"Could not find a configuration file at {self.configurationFileName}, copy {self.configurationFileName}.default to this location and customize with your settings if you do not want the defaults.")

        self.configurationLock.release()
        return self.configuration

    def read_current(self):
        self.currentLock.acquire()
        current = None

        try:
            currentFile = open(self.currentFileName)

            if currentFile.readable():
                current = {}
                for line in currentFile:
                    line = line.strip()
                    if "" != line and "#" != line[0]:
                        pair = line.split()
                        if 1 < len(pair) and ("mode" == pair[0] or "temperature" == pair[0]):
                            current[pair[0]] = pair[1]

            currentFile.close()
        except FileNotFoundError:
            current = { "mode": "off", "temperature": "70.0" }

        self.currentLock.release()
        return current

    def write_current(self, mode, temperature):
        self.currentLock.acquire()
        # ensure values being written are valid
        if mode not in [ "off", "fan", "ac", "heat" ]:
            mode = "off"
        if temperature < self.configuration.minimumTemperature:
            temperature = self.configuration.minimumTemperature
        elif temperature > self.configuration.maximumTemperature:
            temperature = self.configuration.maximumTemperature

        currentFile = open(self.currentFileName, "w")

        if currentFile.writable():
            currentFile.write("mode " + str(mode) + "\ntemperature " + str(temperature))

        self.currentLock.release()
        currentFile.close()

    def read_schedule(self):
        scheduleFileContents = ""
        self.scheduleLock.acquire()

        scheduleFile = open(self.scheduleFileName)
        if scheduleFile.readable():
            scheduleFileContents = scheduleFile.read()

        self.scheduleLock.release()
        return scheduleFileContents

    def add_event(self, day, time, temperature):
        if self.scheduleHelper is not None:
            try:
                event = self.scheduleHelper.add_event(
                    ScheduleEvent(day, time, temperature))
                if event is None:
                    self.write_schedule(self.scheduleHelper.serialize_events())
                return event
            except:
                pass
        return None

    def delete_event(self, day, time):
        if self.scheduleHelper is not None:
            try:
                return self.scheduleHelper.delete_event(day, time)
            except:
                pass
        return None

    def write_schedule(self, scheduleContents):
        self.scheduleLock.acquire()

        scheduleFile = open(self.scheduleFileName, "w")
        if scheduleFile.writable():
            scheduleFile.write(scheduleContents)

        self.scheduleLock.release()

    def set_configuration_option(self, option_name, option_value, configuration):
        if "heat_pin" == option_name:
            configuration.heatPin = int(option_value)
        elif "ac_pin" == option_name:
            configuration.acPin = int(option_value)
        elif "fan_pin" == option_name:
            configuration.fanPin = int(option_value)
        elif "thermometer" == option_name:
            configuration.thermometer = option_value
        elif "thermometer_pin" == option_name:
            configuration.thermometerPin = option_value
        elif "thermometer_test_file" == option_name:
            configuration.thermometerTestFile = option_value
        elif "active_hysteresis" == option_name:
            configuration.activeHysteresis = float(option_value)
        elif "inactive_hysteresis" == option_name:
            configuration.inactiveHysteresis = float(option_value)
        elif "email_alerts" == option_name:
            configuration.emailAlerts = ("true" == option_value.lower())
        elif "email" == option_name:
            configuration.email = option_value
        elif "error_threshold" == option_name:
            configuration.errorThreshold = option_value
        elif "weather_provider" == option_name:
            configuration.weatherProvider = option_value
        elif "weather_api_key" == option_name:
            configuration.weatherAPIKey = option_value
        elif "lat" == option_name:
            configuration.lat = option_value
        elif "long" == option_name:
            configuration.long = option_value
