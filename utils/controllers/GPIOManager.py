__author__ = 'matt'


class GPIOManager():
    def __init__(self, configuration):
        self.configuration = configuration

    def turn_off_fan(self):
        pass

    def turn_on_fan(self):
        pass

    def turn_off_ac(self):
        pass

    def turn_on_ac(self):
        pass

    def turn_off_heat(self):
        pass

    def turn_on_heat(self):
        pass
