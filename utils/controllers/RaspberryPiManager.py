__author__ = 'matt'

from gpiozero import LED

from utils.controllers.GPIOManager import GPIOManager

def SetRelay(relay, on):
    # if on == True, turn relay on by setting it's value low (off)
    if (on):
        relay.off()
    # if on == False, turn relay off by setting it's value high (on)
    else:
        relay.on()

class RaspberryPiManager(GPIOManager):
    def __init__(self, configuration):
        self.fan = LED(pin=configuration.fanPin, initial_value=True)
        self.cool = LED(pin=configuration.acPin, initial_value=True)
        self.heat = LED(pin=configuration.heatPin, initial_value=True)

    def turn_off_fan(self):
        SetRelay(self.fan, False)

    def turn_on_fan(self):
        SetRelay(self.fan, True)

    def turn_off_ac(self):
        SetRelay(self.cool, False)

    def turn_on_ac(self):
        SetRelay(self.cool, True)

    def turn_off_heat(self):
        SetRelay(self.heat, False)

    def turn_on_heat(self):
        SetRelay(self.heat, True)
