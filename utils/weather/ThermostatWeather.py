import time
import threading

class ThermostatWeather():
    def __init__(self, configuration):
        self.apiKey = configuration.weatherAPIKey
        self.lat = configuration.lat
        self.long = configuration.long
        self.weatherFlags = configuration.weatherFlags

        # set up to handle getting daily weather data
        self.lastCheck = time.time()
        self.currentLock = threading.Lock()

        try:
            self.weatherData = self.get_current_data()
        except:
            self.weatherData = None

    def get_current_data(self):
        return None

    def CurrentData(self, type):
        n = 900 # 15 minutes

        # if requesting today's forecast, we do not need such recent queries
        if 'today' == type:
            n = 3600 # 1 hour

        # cache data and only update at intervals
        if n <= int(time.time() - self.lastCheck):
            self.currentLock.acquire()

            self.weatherData = self.get_current_data()

            self.lastCheck = time.time()
            self.currentLock.release()

        return self.weatherData

    def current_weather(self):
        return None

    def today_forecast(self):
        return None
