import urllib.request
import time
import json
import threading

from utils.weather.ThermostatWeather import ThermostatWeather

class DarkSkyWeather(ThermostatWeather):
    def __init__(self, configuration):
        # set up for forecast.io api call
        self.url = "https://api.darksky.net/forecast"
        super().__init__(configuration)

    def get_current_data(self):
        # get current time in seconds
        currentTime = time.strftime('%s')

        # create the url to fetch from provided settings
        url = f"{self.url}/{self.apiKey}/{self.lat},{self.long},{currentTime}?{self.weatherFlags}"

        # request data
        current = urllib.request.urlopen(url).read()

        # parse into dictionary
        weatherData = json.loads(current.decode("utf-8"))

        # daily's only element is an array with only one element... another dictionary...
        # it is silly, just make daily point to the sub dictionary
        weatherData['daily'] = weatherData['daily']['data'][0]

        return weatherData

    def current_weather(self):
        try:
            # extract current weather data from CurrentData
            currently = self.CurrentData('current')['currently']

            # return the apparent temperature and a summary
            return {
                'temperature': round(currently['apparentTemperature'], 1),
                'summary': currently['summary']
            }
        except:
            return None

    def today_forecast(self):
        try:
            # extract today's forcast from CurrentData
            today = self.CurrentData('today')['daily']

            # return a summary of today's weather, the apparent high and low, and the
            # probability of precipitation
            return {
                'summary': today['summary'],
                'apparentTemperatureMax': round(today['apparentTemperatureMax'], 1),
                'apparentTemperatureMin': round(today['apparentTemperatureMin'], 1),
                'precipProbability': today['precipProbability'],
            }
        except:
            return None
