import urllib.request
import time
import json
import threading

from utils.weather.ThermostatWeather import ThermostatWeather

def kToF(kelvin):
    return 9 * (kelvin - 273.15) / 5 + 32

class OpenWeatherMap(ThermostatWeather):
    def __init__(self, configuration):
        # set up for openweathermap.org api call
        self.url = "https://api.openweathermap.org/data/2.5"
        super().__init__(configuration)

    def get_current_data(self):
        # get current time in seconds
        currentTime = time.strftime('%s')

        # create the url to fetch from provided settings
        url = f"{self.url}/onecall?lat={self.lat}&lon={self.long}&appid={self.apiKey}"
        # request data
        current = urllib.request.urlopen(url).read()

        # parse into dictionary
        return json.loads(current.decode("utf-8"))


    def current_weather(self):
        try:
            # extract current weather data from CurrentData
            currently = self.CurrentData('current')['current']

            temperature = kToF(currently['feels_like'])
            humidity = round(currently['humidity'], 1)

            # return the apparent temperature and a summary
            return {
                'temperature': round(temperature, 1),
                'humidity': humidity,
                'summary': currently['weather'][0]['description']
            }
        except:
            return None

    def today_forecast(self):
        try:
            # extract today's forcast from CurrentData
            today = self.CurrentData('today')['daily'][0]

            apparentTemperatureMax = kToF(today['temp']['max'])
            apparentTemperatureMin = kToF(today['temp']['min'])

            # return a summary of today's weather, the apparent high and low, and the
            # probability of precipitation
            return {
                'summary': today['weather'][0]['description'],
                'apparentTemperatureMax': round(apparentTemperatureMax, 1),
                'apparentTemperatureMin': round(apparentTemperatureMin, 1),
                'precipProbability': 'unknown',
            }
        except:
            return None
