__author__ = 'matt'

import logging
logger = logging.getLogger(__name__)

from datetime import datetime, timedelta

import pkgutil
import threading
import time

def time_period_is_exceeded(startDateTime, period):
    if startDateTime is not None:
        timeSinceStart = datetime.now() - startDateTime
        return period < timeSinceStart.seconds
    return False

acString = "ac"
fanString = "fan"
heatString = "heat"

class ThermostatService(threading.Thread):
    def __init__(self, threadId, name, configuration, fileManager, temperatureReader, testMode):
        threading.Thread.__init__(self)
        self.threadId = threadId
        self.name = name

        if not testMode and pkgutil.find_loader("gpiozero") is not None:
            logger.info("Found gpiozero")
            from utils.controllers.RaspberryPiManager import RaspberryPiManager as GPIOController
        elif not testMode and pkgutil.find_loader("Adafruit_BBIO") is not None:
            logger.info("Found Adafruit_BBIO")
            from utils.controllers.BeagleBoneBlackGPIOManager import BeagleBoneBlackGPIOManager as GPIOController
        else:
            logger.info("Could not find hardware controller, using TestGPIOManager")
            from utils.controllers.GPIOManager import GPIOManager as GPIOController

        self.configuration = configuration
        self.fileManager = fileManager
        self.manager = GPIOController(configuration)

        self.temperatureReader = temperatureReader
        self.lastTempReadAt = datetime.now()
        self.tempReadFailureTimeout = 60

        self.guard = None
        self.timeoutPeriod = 300
        self.sleepPeriod = 5

        _current = fileManager.read_current()
        self.currentMode = _current["mode"]
        self.currentTemperature = _current["temperature"]

    def run(self):
        logger.info("Starting service")
        self.turn_off_heat()
        self.turn_off_ac()
        self.turn_off_fan()

        try:
            while True:
                logger.info(time.strftime("%H:%M:%S ") + "thermostat service polling...")

                # check current
                newValues = self.fileManager.read_current()
                temperature = self.temperatureReader.CurrentTemperature()

                if temperature is None:
                    if time_period_is_exceeded(self.lastTempReadAt, self.tempReadFailureTimeout):
                        raise RuntimeError(f"Service has not read a good value from the thermometer in {self.tempReadFailureTimeout} seconds, entering failure mode!")
                    else:
                        timeSinceLastRead = (datetime.now() - self.lastTempReadAt).seconds
                        logger.warning(f"Service has not read a good value from the thermometer in {timeSinceLastRead} seconds - continuing with old settings until timeout of {self.tempReadFailureTimeout} seconds is exceeded.")

                else:
                    self.lastTempReadAt = datetime.now()
                    self.currentMode = newValues["mode"]
                    self.currentTemperature = round(float(newValues["temperature"]), 1)

                    if heatString == self.currentMode:
                        self.turn_off_ac()

                        if temperature < self.currentTemperature -\
                            self.configuration.inactiveHysteresis:

                            self.turn_on_heat()

                        elif temperature > self.currentTemperature +\
                            self.configuration.activeHysteresis:

                            self.turn_off_heat()

                        self.turn_off_fan() # if heat is on, this will be ignored

                    elif acString == self.currentMode:
                        self.turn_off_heat()

                        if temperature > self.currentTemperature +\
                            self.configuration.inactiveHysteresis:

                            self.turn_on_ac()

                        elif temperature < self.currentTemperature -\
                            self.configuration.activeHysteresis:

                            self.turn_off_ac()

                        self.turn_off_fan() # if ac is on, this will be ignored

                    elif fanString == self.currentMode:
                        self.turn_on_fan()
                        self.turn_off_heat()
                        self.turn_off_ac()

                    else:
                        self.turn_off_fan()
                        self.turn_off_heat()
                        self.turn_off_ac()

                time.sleep(self.sleepPeriod)

        except Exception as e:
            #TODO: handle an error in mode changing
            # error! maybe try handling at some point, but first just turn off
            # everything except the fan.
            self.turn_on_fan()
            self.turn_off_heat()
            self.turn_off_ac()

            logger.critical("Service got an exception, turning fan on and other systems off and exiting service...")
            raise

    def turn_off_fan(self):
        if fanString in self.configuration.running and \
            acString not in self.configuration.running and \
            heatString not in self.configuration.running:

            if self.guard is None or \
                time_period_is_exceeded(self.guard["changedAt"], self.timeoutPeriod):

                logger.info(f"turning {fanString} off")
                self.guard = None
                self.configuration.running.remove(fanString)
                self.manager.turn_off_fan()

    def turn_on_fan(self):
        if fanString not in self.configuration.running:
            logger.info(f"turning {fanString} on")
            self.configuration.running.append(fanString)
            self.manager.turn_on_fan()

    def turn_off_ac(self):
        if acString in self.configuration.running:
            logger.info(f"turning {acString} off")
            self.guard = {
                "changedAt": datetime.now(),
                "changedFrom": acString
            }
            self.configuration.running.remove(acString)
            self.manager.turn_off_ac()

    def turn_on_ac(self):
        if acString not in self.configuration.running and \
            (self.guard is None or \
            self.guard["changedFrom"] == acString or \
            time_period_is_exceeded(self.guard["changedAt"], self.timeoutPeriod)):

            logger.info(f"turning {acString} on")
            self.guard = None
            self.turn_on_fan()
            self.configuration.running.append(acString)
            self.manager.turn_on_ac()

    def turn_off_heat(self):
        if heatString in self.configuration.running:
            logger.info(f"turning {heatString} off")
            self.guard = {
                "changedAt": datetime.now(),
                "changedFrom": heatString
            }
            self.configuration.running.remove(heatString)
            self.manager.turn_off_heat()

    def turn_on_heat(self):
        if heatString not in self.configuration.running and \
            (self.guard is None or \
            self.guard["changedFrom"] == heatString or \
            time_period_is_exceeded(self.guard["changedAt"], self.timeoutPeriod)):

            logger.info(f"turning {heatString} on")
            self.guard = None
            self.turn_on_fan()
            self.configuration.running.append(heatString)
            self.manager.turn_on_heat()
