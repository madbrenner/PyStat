__author__ = 'matt'

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('ThermostatMain')

import time
import threading

from service.ThermostatService import ThermostatService
from scheduler.ThermostatScheduler import ThermostatScheduler
from utils.FileManager import FileManager
from utils.ThermostatDatabase import ThermostatDatabase
from web.ThermostatWeb import ThermostatWeb

def main():

    # magic constants
    #configurationFileName = "../../PyStat/thermostat.conf"
    configurationFileName = "thermostat.conf"
    currentFileName = "current.set"
    scheduleFileName = "scheduler/schedule.conf"
    databaseName = "pystat.db"

    # make the static utility classes
    fileManager = FileManager(configurationFileName, currentFileName, scheduleFileName)
    configuration = fileManager.read_configuration()
    logger.info(f"Checking to use {configuration.thermometer} thermometer")

    if "DS18B20" == configuration.thermometer:
        from utils.thermometers.DS18B20 import DS18B20
        temperatureReader = DS18B20(configuration)
    elif str.startswith(configuration.thermometer, "DHT"):
        from utils.thermometers.DHT import DHT
        temperatureReader = DHT(configuration)
    elif "MCP9808" == configuration.thermometer:
        from utils.thermometers.MCP9808 import MCP9808
        temperatureReader = MCP9808(configuration)
    else:
        logger.info("No valid thermometer specified, will read from dummy file.")
        from utils.thermometers.TemperatureReader import TemperatureReader
        temperatureReader = TemperatureReader(configuration)

    if "darksky" == configuration.weatherProvider:
        logger.info("Using DarkSky as weather provider")
        from utils.weather.DarkSkyWeather import DarkSkyWeather
        weather = DarkSkyWeather(configuration)
    elif "openweathermap" == configuration.weatherProvider:
        logger.info("Using OpenWeatherMap as weather provider")
        from utils.weather.OpenWeatherMap import OpenWeatherMap
        weather = OpenWeatherMap(configuration)
    else:
        logger.info("No weather provider specified")
        from utils.weather.ThermostatWeather import ThermostatWeather
        weather = ThermostatWeather(configuration)

    # create the database utility class
    databaseHelper = ThermostatDatabase(databaseName)

    if configuration is None:
        logger.info("Configuration file could not be read.")
        exit(1)

    testMode = False
    if "-1" == configuration.heatPin \
            or "-1" == configuration.acPin \
            or "-1" == configuration.fanPin \
            or configuration.heatPin == configuration.acPin \
            or configuration.heatPin == configuration.fanPin \
            or configuration.acPin == configuration.fanPin:
        logger.info("The pin settings provided are invalid:")
        logger.info("\tHeat pin: " + str(configuration.heatPin))
        logger.info("\tAC pin: " + str(configuration.acPin))
        logger.info("\tFan pin: " + str(configuration.fanPin))
        logger.info("The test controller will be used.")
        testMode = True

    service = ThermostatService(0, "service", configuration, fileManager,
                                temperatureReader, testMode)
    service.start()

    scheduler = ThermostatScheduler(1, "scheduler", configuration, fileManager,
                                    temperatureReader)
    scheduler.start()

    web = ThermostatWeb(2, "web", configuration, fileManager,
                        temperatureReader, weather)
    web.start()

    while True:
        current = fileManager.read_current()

        currentWeather = weather.current_weather()
        outdoorTemperature = None

        if currentWeather is not None:
            outdoorTemperature = currentWeather['temperature']

        databaseHelper.insert_current_data(temperatureReader.CurrentTemperature(),
                                           current["temperature"], outdoorTemperature,
                                           current["mode"],
                                           ("heat" in configuration.running),
                                           ("ac" in configuration.running),
                                           ("fan" in configuration.running))

        logger.info("Polling threads")
        if not service.is_alive():
            logger.info("PANIC SERVICE IS DEAD")
        if not scheduler.is_alive():
            logger.info("PANIC SCHEDULER IS DEAD")
        if not web.is_alive():
            logger.info("PANIC WEB IS DEAD")

        time.sleep(30)

main()
