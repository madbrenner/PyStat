#!/bin/sh

xset -dpms
xset s off
xset s noblank

#matchbox-window-manager &
openbox &

unclutter -idle 0.5 -root &

while :
do
    midori -e Fullscreen -a http://127.0.0.1:5000
done
